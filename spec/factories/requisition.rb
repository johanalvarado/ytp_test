FactoryGirl.define do
  factory :requisition do
    income { Faker::Number.number(5)}
    address_years { Faker::Number.number(2)}
    company_years{ Faker::Number.number(2)}
    marital_status{ 'single' }
    requested_amount{ Faker::Number.number(4)}
    payment_terms{ Faker::Lorem.characters(35)}
    bank{ Faker::Bank.name}
    comment{ Faker::Lorem.characters(35)}
    company_name{ Faker::Company.name }
    company_position{ Faker::Lorem.word}
    company_phone_number{ Faker::PhoneNumber.cell_phone }
    dependents_number{ Faker::Number.number(2) }
    has_sgmm{ Faker::Boolean.boolean(0.2)}
    has_imss{ Faker::Boolean.boolean(0.2)}
    has_car{ Faker::Boolean.boolean(0.2)}

    association :user, factory: :user, strategy: :build

    factory :invalid_requisition do
      income nil
    end
  end
end
