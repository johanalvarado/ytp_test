FactoryGirl.define do
  factory :profile do
    first_name { Faker::Name.first_name }
    second_name{ Faker::Name.first_name }
    first_last_name{ Faker::Name.last_name}
    second_last_name{ Faker::Name.last_name }
    birth_date{ Time.now }
    curp{ 'BADD110313HCMLNS09' }
    rfc{ 'BADD110313'}
    gender{ 'female' }
    birth_state{ 'Guadalajara' }
    phone_number{Faker::PhoneNumber.cell_phone }
    email{ Faker::Internet.email }


    factory :invalid_profile do
      first_name nil
    end
  end
end
