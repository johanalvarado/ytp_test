FactoryGirl.define do
  factory :user do
    facebook_id { Faker::Number.number(10) }
    name{ Faker::Name.first_name }
    oauth_token{ Faker::Name.last_name}
    oauth_expires_at{ Time.now }
  end
end
