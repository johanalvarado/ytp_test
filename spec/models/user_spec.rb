require 'rails_helper'

RSpec.describe User, :type => :model do
  it { should have_one(:profile) }

  it { should have_many(:requisitions) }

  it "is not valid with empty attributes" do
    expect(User.create).to_not be_valid
  end

  it "is not valid without a name" do
    expect(User.create(name: nil)).to_not be_valid
  end

  it "is not valid without a facebook_id" do
    expect(User.create(facebook_id: nil)).to_not be_valid
  end

  it "is not valid without a oauth_token" do
    expect(User.create(oauth_token: nil)).to_not be_valid
  end

  it "is not valid without a oauth_expires_at" do
    expect(User.create(oauth_expires_at: nil)).to_not be_valid
  end

  it "is valid with valid attributes" do
    expect(User.create(name: 'Test name',
                       facebook_id: '1234',
                       oauth_token: '1234',
                       oauth_expires_at: Time.now
                      )).to be_valid
  end
end
