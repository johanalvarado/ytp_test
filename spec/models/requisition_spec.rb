require 'rails_helper'

RSpec.describe Requisition, :type => :model do
  it { should belong_to(:user) }

  it { should have_many(:personal_references) }

  it { should accept_nested_attributes_for(:personal_references) }

  it "is not valid with empty attributes" do
    expect(Requisition.create).to_not be_valid
  end

  it "is not valid without an income" do
    expect(Requisition.create(income: nil)).to_not be_valid
  end

  it "is not valid without an address_years" do
    expect(Requisition.create(address_years: nil)).to_not be_valid
  end

  it "is not valid without a company_years" do
    expect(Requisition.create(company_years: nil)).to_not be_valid
  end

  it "is not valid without a marital_status" do
    expect(Requisition.create(marital_status: nil)).to_not be_valid
  end

  it "is not valid without a requested_amount" do
    expect(Requisition.create(requested_amount: nil)).to_not be_valid
  end

  it "is not valid without a payment_terms" do
    expect(Requisition.create(payment_terms: nil)).to_not be_valid
  end

  it "is not valid without a bank" do
    expect(Requisition.create(bank: nil)).to_not be_valid
  end

  it "is not valid without a comment" do
    expect(Requisition.create(comment: nil)).to_not be_valid
  end

  it "is not valid without a company_name" do
    expect(Requisition.create(company_name: nil)).to_not be_valid
  end

  it "is not valid without a company_phone_number" do
    expect(Requisition.create(company_phone_number: nil)).to_not be_valid
  end

  it "is not valid without a dependents_number" do
    expect(Requisition.create(dependents_number: nil)).to_not be_valid
  end
  it "is not valid without a company_position" do
    expect(Requisition.create(company_position: nil)).to_not be_valid
  end
end
