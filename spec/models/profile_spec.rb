require 'rails_helper'

RSpec.describe Profile, :type => :model do
  it "is not valid with empty attributes" do
    expect(Profile.create).to_not be_valid
  end

  it "is not valid without a first_name" do
    expect(Profile.create(first_name: nil)).to_not be_valid
  end

  it "is not valid without a second_name" do
    expect(Profile.create(second_name: nil)).to_not be_valid
  end

  it "is not valid without a second_last_name" do
    expect(Profile.create(second_last_name: nil)).to_not be_valid
  end

  it "is not valid without a first_last_name " do
    expect(Profile.create(first_last_name: nil)).to_not be_valid
  end

  it "is not valid without a birth_date" do
    expect(Profile.create(birth_date: nil)).to_not be_valid
  end

  it "is not valid without a curp" do
    expect(Profile.create(curp: nil)).to_not be_valid
  end

  it "is not valid without a rfc" do
    expect(Profile.create(rfc: nil)).to_not be_valid
  end
  it "is not valid without a gender" do
    expect(Profile.create(gender: nil)).to_not be_valid
  end
  it "is not valid without a birth_state" do
    expect(Profile.create(birth_state: nil)).to_not be_valid
  end
  it "is not valid without a phone_number" do
    expect(Profile.create(phone_number: nil)).to_not be_valid
  end
end
