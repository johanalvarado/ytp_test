require 'rails_helper'

RSpec.describe PersonalReference, :type => :model do
  it { should belong_to(:requisition) }

  it "is not valid with empty attributes" do
    expect(PersonalReference.create).to_not be_valid
  end

  it "is not valid without a first_name" do
    expect(PersonalReference.create(first_name: nil)).to_not be_valid
  end

  it "is not valid without a second_name" do
    expect(PersonalReference.create(second_name: nil)).to_not be_valid
  end

  it "is not valid without a second_last_name" do
    expect(PersonalReference.create(second_last_name: nil)).to_not be_valid
  end

  it "is not valid without a first_last_name " do
    expect(PersonalReference.create(first_last_name: nil)).to_not be_valid
  end

  it "is not valid without a birth_date" do
    expect(PersonalReference.create(cell_phone_number: nil)).to_not be_valid
  end
end
