require 'rails_helper'
require 'pry'

describe RequisitionsController do
  before(:each) do
    session[:user_id] = create(:user).id

    @personal_references = [
      attributes_for(:personal_reference),
      attributes_for(:personal_reference),
      attributes_for(:personal_reference)
    ]
  end

  describe 'GET #new' do
    it "assigns a new Requisition to @requisition" do
      get :new
      expect(assigns(:requisition)).to be_a_new(Requisition)
    end
    it "renders the :new template" do
      get :new
      expect(response).to render_template :new
    end
  end

  describe "POST #create" do
    context "with valid attributes" do
      it "saves the new requisition in the database" do
        expect{
          post :create, requisition: attributes_for(:requisition,
                                                    personal_references_attributes: @personal_references)

        }.to change(Requisition.all, :count).by(1)
      end
    end

    context "with invalid attributes" do
      it "does not save the new requisition in the database" do
        expect{
          post :create, requisition: attributes_for(:invalid_requisition,
                                                    personal_references_attributes: @personal_references
                                                   )
        }.not_to change(Requisition, :count)
      end
      it "re-renders the :new template" do
        post :create,
          requisition: attributes_for(:invalid_requisition)
        expect(response).to render_template :new
      end
    end
  end
end
