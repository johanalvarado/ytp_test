require 'rails_helper'

describe ProfilesController do
  before(:each) do
    session[:user_id] = create(:user).id
  end

  describe 'GET #new' do
    it "assigns a new Profile to @profile" do
      get :new
      expect(assigns(:profile)).to be_a_new(Profile)
    end
    it "renders the :new template" do
      get :new
      expect(response).to render_template :new
    end
  end

  describe "POST #create" do
    context "with valid attributes" do
      it "saves the new profile in the database" do
        expect{
          post :create, profile: attributes_for(:profile)
        }.to change(Profile, :count).by(1)
      end
    end

    context "with invalid attributes" do
      it "does not save the new profile in the database" do
        expect{
          post :create, profile: attributes_for(:invalid_profile)
        }.not_to change(Profile, :count)
      end
      it "re-renders the :new template" do
        post :create,
          profile: attributes_for(:invalid_profile)
        expect(response).to render_template :new
      end
    end
  end
end
