class RequisitionsController < ApplicationController
  def index
    @requisitions = current_user.requisitions
  end

  def new
    @requisition = current_user.requisitions.build
  end

  def create
    @requisition = current_user.requisitions.build(requisition_params)
    if @requisition.save
      redirect_to root_path
    else
      render action: "new"
    end
  end

  def edit
    @requisition = current_user.requisitions.find(params[:id])
  end

  def update
    @requisition = current_user.requisitions.find(params[:id])
    if @requisition.update_attributes(requisition_params)
      redirect_to root_path
    else
      render 'edit'
    end
  end

  private

  def requisition_params
    params.require(:requisition).permit(
      :income,
      :address_years,
      :company_years,
      :marital_status,
      :requested_amount,
      :payment_terms,
      :bank,
      :comment,
      :company_name,
      :company_phone_number,
      :dependents_number,
      :company_position,
      :has_sgmm,
      :has_imss,
      :has_car,
      personal_references_attributes: [
        :first_name,
        :second_name,
        :first_last_name,
        :second_last_name,
        :cell_phone_number
      ]
    )
  end
end
