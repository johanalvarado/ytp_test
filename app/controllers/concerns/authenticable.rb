module Authenticable
  def authenticate_user!
    redirect_to new_session_path unless user_signed_in?
  end

  def current_user
    @current_user ||= User.find(session[:user_id])
  end

  def public_profile
    facebook_connection = Koala::Facebook::API.new(current_user.oauth_token)
    return {} if facebook_connection.app_secret.nil?
    facebook_connection.get_object("me?fields=first_name,birthday,email,gender")
  end

  private
  def user_signed_in?
    session[:user_id].present?
  end
end
