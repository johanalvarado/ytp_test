class ProfilesController < ApplicationController
  def new
    @profile = Profile.new(public_profile)
  end

  def create
    @profile = current_user.create_profile(profile_params)
    if @profile.save
      redirect_to root_path
    else
      render action: "new"
    end
  end

  private

  def profile_params
    params.require(:profile).permit(
      :first_name,
      :second_name,
      :second_last_name,
      :first_last_name,
      :birth_date,
      :curp,
      :rfc,
      :gender,
      :birth_date,
      :phone_number,
      :email,
      :birth_state
    )
  end
end
