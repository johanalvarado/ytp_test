class SessionsController < ApplicationController
  skip_before_action :authenticate_user!, only: [:new, :create]

  def create
    @user = register_user
    session[:user_id] = @user.id
    return redirect_to new_profile_path unless has_profile?
    redirect_to root_path
  end

  private

  def register_user
    User.facebook_authenticate(request.env["omniauth.auth"])
  end

  def has_profile?
    @user.profile.present?
  end
end
