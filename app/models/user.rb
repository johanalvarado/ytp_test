class User < ActiveRecord::Base
  has_one :profile
  has_many :requisitions
  validates :name,
    :facebook_id,
    :oauth_token,
    :oauth_expires_at,
    presence: true

  class << self
    def facebook_authenticate(auth)
      where(facebook_id: auth[:uid]).first_or_initialize.tap do |user|
        user.facebook_id = auth.uid
        user.name = auth.info.name
        user.oauth_token = auth.credentials.token
        user.oauth_expires_at = Time.at(auth.credentials.expires_at)
        user.save!
      end
    end
  end
end
