class Requisition < ActiveRecord::Base
  belongs_to :user
  has_many :personal_references

  validates :income,
    :address_years,
    :company_years,
    :marital_status,
    :requested_amount,
    :payment_terms,
    :bank,
    :comment,
    :company_name,
    :company_phone_number,
    :dependents_number,
    :company_position,
    presence: true

  accepts_nested_attributes_for :personal_references
end
