class PersonalReference < ActiveRecord::Base
  belongs_to :requisition

  validates :first_name,
    :second_name,
    :first_last_name,
    :second_last_name,
    :cell_phone_number,
    presence: true
end
