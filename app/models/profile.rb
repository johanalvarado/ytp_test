class Profile < ActiveRecord::Base
  validates :first_name,
    :second_name,
    :second_last_name,
    :first_last_name,
    :birth_date,
    :curp,
    :rfc,
    :gender,
    :birth_state,
    :phone_number,
    :email,
    presence: true

  validates :email,
    :rfc,
    :curp,
    uniqueness: true
end
