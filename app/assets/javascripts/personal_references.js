var viewModel = function(items) {
  self = this;
  self.items = ko.observableArray(items);
  self.first_name = ko.observable();
  self.second_name = ko.observable();
  self.first_last_name = ko.observable();
  self.second_last_name = ko.observable();
  self.cell_phone_number = ko.observable();

  self.addPersonalReference = function() {
    console.log(self.first_name());
    if(self.first_name() != undefined && self.second_name() != undefined && self.first_last_name() != undefined && self.second_last_name() != undefined && self.cell_phone_number() != undefined){
      if(self.first_name() != "" && self.second_name() != "" && self.first_last_name() != "" && self.second_last_name() != "" && self.cell_phone_number() != ""){
        self.items.push({
          first_name: self.first_name(),
          second_name: self.second_name(),
          first_last_name: self.first_last_name(),
          second_last_name: self.second_last_name(),
          cell_phone_number: self.cell_phone_number()
        });
        len = self.items().length - 1;
        $('#itemPersonalReferences').append('<input id="requisition_personal_references_attributes_'+len+'_first_name" name="requisition[personal_references_attributes]['+len+'][first_name]" type="text" value="'+self.first_name()+'">'+
                                            '<input id="requisition_personal_references_attributes_'+len+'_second_name" name="requisition[personal_references_attributes]['+len+'][second_name]" type="text" value="'+self.second_name()+'">'+
                                              '<input id="requisition_personal_references_attributes_'+len+'_first_last_name" name="requisition[personal_references_attributes]['+len+'][first_last_name]" type="text" value="'+self.first_last_name()+'">'+
                                                '<input id="requisition_personal_references_attributes_'+len+'_second_last_name" name="requisition[personal_references_attributes]['+len+'][second_last_name]" type="text" value="'+self.second_last_name()+'">'+
                                                  '<input id="requisition_personal_references_attributes_'+len+'_cell_phone_number" name="requisition[personal_references_attributes]['+len+'][cell_phone_number]" type="text" value="'+self.cell_phone_number()+'">'
                                           )
         self.first_name("");
         self.second_name("");
         self.first_last_name("");
         self.second_last_name("");
         self.cell_phone_number("");
         $('#form_erros').text("");
      }else{
        $('#form_errors').text("Debes llenar todos los campos");
      }
    }else{
      $('#form_errors').text("Debes llenar todos los campos");
    }
  };
};
