$(document).ready(function(){
  $('form#new_profile').validator()

  $("input.uppercase").keyup(function(){
    $(this).val( $(this).val().toUpperCase() );
  });

  $('form#new_profile').validator().on('submit', function (e) {
    if (e.isDefaultPrevented()) {
      e.preventDefault();
    }
  })
});
