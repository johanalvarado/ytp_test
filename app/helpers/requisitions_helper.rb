module RequisitionsHelper
  def welcome_message
    if @requisitions.empty?
      t('requisitions.messages.empty_requisitions')
    else
      current_user.name
    end
  end

  def options_of_marital_status
    {'Soltero' => 'single', 'Casado' => 'married'}
  end
end
