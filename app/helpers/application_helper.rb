module ApplicationHelper
  def boolean_options
    {"SI" => true, "NO" => false}
  end

  def current_user
    @current_user ||= User.find(session[:user_id])
  end
end
