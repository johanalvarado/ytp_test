Rails.application.routes.draw do
  root 'requisitions#index'

  resources :sessions, only: [:new]

  resources :profiles, only: [:new, :create]

  resources :requisitions, except: [:destroy]

  match 'auth/:provider/callback', to: 'sessions#create', via: [:get, :post]
end
