# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170123192623) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "personal_references", force: true do |t|
    t.string   "first_name"
    t.string   "second_name"
    t.string   "first_last_name"
    t.string   "second_last_name"
    t.string   "cell_phone_number"
    t.integer  "requisition_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "personal_references", ["requisition_id"], name: "index_personal_references_on_requisition_id", using: :btree

  create_table "profiles", force: true do |t|
    t.string   "first_name"
    t.string   "second_name"
    t.string   "second_last_name"
    t.string   "first_last_name"
    t.date     "birth_date"
    t.string   "curp"
    t.string   "rfc"
    t.string   "gender"
    t.string   "birth_state"
    t.string   "phone_number"
    t.string   "email"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "profiles", ["user_id"], name: "index_profiles_on_user_id", using: :btree

  create_table "requisitions", force: true do |t|
    t.float    "income"
    t.integer  "address_years"
    t.integer  "company_years"
    t.string   "marital_status"
    t.float    "requested_amount"
    t.string   "payment_terms"
    t.string   "bank"
    t.string   "comment"
    t.string   "company_name"
    t.string   "company_phone_number"
    t.integer  "dependents_number"
    t.string   "company_position"
    t.boolean  "has_sgmm"
    t.boolean  "has_imss"
    t.boolean  "has_car"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "requisitions", ["user_id"], name: "index_requisitions_on_user_id", using: :btree

  create_table "users", force: true do |t|
    t.string   "facebook_id"
    t.string   "name"
    t.string   "oauth_token"
    t.datetime "oauth_expires_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
